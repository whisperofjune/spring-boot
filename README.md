# spring-boot

#### 介绍
1、Springboot整合redis监听键值失效回调函数。
2、Springboot整合redis哨兵机制。
3、Springboot整合redis集群哨兵机制。
4、Springboot整合布隆过滤器解决缓存穿透问题。
5、基于Redis实现分布式锁，
6、Springboot整合RabbitMQ
7、基于Rabittmq实现分布式事务
8、Springboot整合kafka
9、Springboot整合Rocketmq
10、基于RocketMq实现分布式事务
11、Springboot整合XXl-Job
12、Springboot整合ElasticSearch
13、Springboot整合ELK
#### 软件架构
软件架构说明

#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

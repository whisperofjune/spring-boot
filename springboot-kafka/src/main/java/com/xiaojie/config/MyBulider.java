package com.xiaojie.config;

import org.apache.kafka.common.security.auth.AuthenticationContext;
import org.apache.kafka.common.security.auth.KafkaPrincipal;
import org.apache.kafka.common.security.auth.KafkaPrincipalBuilder;

/**
 * @Description: peizhi
 * @author: yan
 * @date: 2021.10.26
 */
    /*
    Kafka client 是一个用户的概念， 是在一个安全的集群中经过身份验证的用户。
    在一个支持非授权客户端的集群中，用户是一组非授权的 users，
    broker使用一个可配置的 PrincipalBuilder 类来配置 group 规则。
     Client-id 是客户端的逻辑分组，客户端应用使用一个有意义的名称进行标识。
     (user, client-id)元组定义了一个安全的客户端逻辑分组，使用相同的user 和 client-id 标识。
    资源配额可以针对 （user,client-id），users 或者client-id groups 三种规则进行配置。
    对于一个请求连接，连接会匹配最细化的配额规则的限制。
    同一个 group 的所有连接共享这个 group 的资源配额。
    举个例子，如果 (user="test-user", client-id="test-client")
    客户端producer 有10MB/sec 的生产资源配置，这10MB/sec 的资源在所有 "test-user" 用户，
    client-id是 "test-client" 的producer实例中是共享的。
     */
public class MyBulider implements KafkaPrincipalBuilder {

    @Override
    public KafkaPrincipal build(AuthenticationContext context) {
        return null;
    }
}

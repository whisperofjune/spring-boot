package com.xiaojie.service.message.service;


import com.xiaojie.service.message.dto.AlarmMessageDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 发送通知的接口
 * @date 2022/4/29 17:28
 */
public interface MessageService {

    @GetMapping("/send/{name}")
    void send(@PathVariable(value = "name") String name);

    /**
     * @description: 监控系统异常通知, 为post请求
     * @param:
     * @param: jsonpObject
     * @return: void
     * @author xiaojie
     * @date: 2022/5/2 18:36
     */
    @PostMapping("/notify")
    void send(@RequestBody List<AlarmMessageDto> alarmMessageList);
}

package com.xiaojie.service.message.dto;

import lombok.Data;

/**
 * @description: skywalking 报警系统实体类
 * 数据格类似这样的
 * [{
 *     "scopeId": 1,
 *     "scope": "SERVICE",
 *     "name": "serviceA",
 *     "id0": 12,
 *     "id1": 0,
 *     "ruleName": "service_resp_time_rule",
 *     "alarmMessage": "alarmMessage xxxx",
 *     "startTime": 1560524171000
 * }, {
 *     "scopeId": 1,
 *     "scope": "SERVICE",
 *     "name": "serviceB",
 *     "id0": 23,
 *     "id1": 0,
 *     "ruleName": "service_resp_time_rule",
 *     "alarmMessage": "alarmMessage yyy",
 *     "startTime": 1560524171000
 * }]
 * scopeId、scope：所有可用的 Scope 详见
 * org.apache.skywalking.oap.server.core.source.DefaultScopeDefine
 * name：目标 Scope 的实体名称
 * id0：Scope 实体的 ID
 * id1：保留字段，目前暂未使用
 * ruleName：告警规则名称
 * alarmMessage：告警消息内容
 * startTime：告警时间，格式为时间戳
 *
 * @author xiaojie
 * @date 2022/5/2 18:46
 * @version 1.0
 */
@Data
public class AlarmMessageDto {
    private String scopeId;
    private String name;
    private String id0;
    private String id1;
    private String alarmMessage;
    private long startTime;
}
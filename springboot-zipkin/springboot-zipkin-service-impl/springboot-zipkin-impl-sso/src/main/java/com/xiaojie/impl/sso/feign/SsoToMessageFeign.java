package com.xiaojie.impl.sso.feign;

import com.xiaojie.service.message.service.MessageService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @author xiaojie
 * @version 1.0
 * @description:feign客户端调用message
 * @date 2022/4/29 17:39
 */
@FeignClient("xiaojie-message")
public interface SsoToMessageFeign extends MessageService {
}

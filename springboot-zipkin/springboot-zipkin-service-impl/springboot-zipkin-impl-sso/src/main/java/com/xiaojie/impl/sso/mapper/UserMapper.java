package com.xiaojie.impl.sso.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xiaojie.impl.sso.entity.User;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/5/2 16:16
 */
public interface UserMapper extends BaseMapper<User> {
}

package com.xiaojie.impl.sso;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.sleuth.zipkin2.ZipkinProperties;
import org.springframework.cloud.sleuth.zipkin2.ZipkinRestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import zipkin2.reporter.Sender;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/4/29 17:57
 */
@SpringBootApplication
@EnableFeignClients
@MapperScan("com.xiaojie.impl.sso.mapper")
public class SsoApp {

    public static void main(String[] args) {
        SpringApplication.run(SsoApp.class);
    }
}

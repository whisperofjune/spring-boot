package com.xiaojie.impl.sso.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.xiaojie.impl.sso.entity.User;
import com.xiaojie.impl.sso.feign.SsoToMessageFeign;
import com.xiaojie.impl.sso.mapper.UserMapper;
import com.xiaojie.service.sso.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/4/29 17:35
 */
@RestController
@Slf4j
public class LoginServiceImpl implements LoginService {
    @Autowired
    private SsoToMessageFeign ssoToMessageFeign;
    @Autowired
    private UserMapper userMapper;

    @Override
    public String login(String name, String pwd) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String traceId = request.getHeader("x-b3-traceid");
        String spanId = request.getHeader("x-b3-spanid");
        String parentSpanId = request.getHeader("x-b3-parentspanid");
        log.info("sso---------traceId:>>>>>>{},spanId>>>>>>{},parentSpanId>>>>>>{}", traceId, spanId, parentSpanId);

        QueryWrapper<User> wrapper = new QueryWrapper<>();
        wrapper.eq("name", name);
        wrapper.eq("pwd", pwd);
        User user = userMapper.selectOne(wrapper);
        if (null != user) {
            ssoToMessageFeign.send(name);
//            int i=1/0; //模拟报错
            try {
                Thread.sleep(1500); //模拟时长
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return name + "登录成功，登陆时间为" + new Date();
        }
        return "登陆失败，账号密码错误" + new Date();
    }
}

package com.xiaojie.service;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/10/11 22:07
 */
public interface OrderService {
    /**
     * @description: 订单创建成功之后，调用派单系统去派单
     * @param:
     * @param: orderId
     * @return: java.lang.String
     * @author xiaojie
     * @date: 2021/10/11 22:08
     */
    String saveOrder();
}

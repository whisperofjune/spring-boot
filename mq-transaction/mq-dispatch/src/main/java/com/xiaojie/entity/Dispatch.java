package com.xiaojie.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 派单
 * @date 2021/10/11 22:48
 */
@Data
public class Dispatch {
    private Long id;
    //订单编号
    private String orderId;
    //用户id
    private Long userId;
    //骑士id
    private Long riderId;
    //派单状态0派单中，1-派单完成，2-派单失败
    private Integer status;
    //创建时间
    private Date createTime;
    //预计送单时长
    private Long sendTime;

}

/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : localhost:3306
 Source Schema         : my_dispatch

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 12/10/2021 10:00:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_dispatch
-- ----------------------------
DROP TABLE IF EXISTS `tb_dispatch`;
CREATE TABLE `tb_dispatch`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `orderId` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `userId` bigint NULL DEFAULT NULL,
  `riderId` bigint NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL,
  `createTime` datetime NULL DEFAULT NULL,
  `sendTime` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `orderId`(`orderId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_dispatch
-- ----------------------------
INSERT INTO `tb_dispatch` VALUES (1, '0602ecbe-36a7-4564-b437-6e1bb260ded5', 15672, 1000012, 0, '2021-10-12 09:55:59', 1800);
INSERT INTO `tb_dispatch` VALUES (2, '65e89412-fdcf-437d-ae5f-dc9466792a50', 15672, 1000012, 0, '2021-10-12 09:57:57', 1800);

SET FOREIGN_KEY_CHECKS = 1;

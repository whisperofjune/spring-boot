package com.xiaojie.job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/11/16 22:22
 */
@SpringBootApplication
public class JobApp {
    public static void main(String[] args) {
        SpringApplication.run(JobApp.class);
    }
}

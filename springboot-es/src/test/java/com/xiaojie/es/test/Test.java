package com.xiaojie.es.test;

import org.apache.commons.lang3.RandomStringUtils;

/**
 * @Description:
 * @author: yan
 * @date: 2021.11.30
 */
public class Test {


    /*
    andom(int count) 生成一个长度为count的字符串，随机内容包含全部的编码。

    randomAscii(int count) 生成一个长度为count的字符串，随机内容仅包含Ascii编码。

    randomAlphabetic(int count) 生成一个长度为count的字符串，随机内容包含字母（含大小）的编码。

    randomAlphanumeric(int count) 生成一个长度为count的字符串，随机内容包含字母（含大小）数字（0～9）的编码。

    randomNumeric(int count) 生成一个长度为count的字符串，随机内容包含数字（0～9）的编码。

    random(int count, char[] chars) 生成一个长度为count的字符串，随机内容包含chars。

    random(int count, String chars) 生成一个长度为count的字符串，随机内容包含chars。
     */
    public static void main(String[] args) {
        System.out.println("全部编码格式。。。。"+RandomStringUtils.random(6));
        System.out.println("Ascii编码。。。。"+RandomStringUtils.randomAscii(6));
        System.out.println("内容包含字母（含大小）的编码。。。。"+RandomStringUtils.randomAlphabetic(6));
        System.out.println("随机内容包含字母（含大小）数字（0～9）的编码。。。。"+RandomStringUtils.randomAlphanumeric(6));
        System.out.println("随机内容包含数字（0～9）的编码。。。。"+RandomStringUtils.randomNumeric(6));
        char[] charArray = { 'a', 'b', 'c', 'd', 'e','f','g' };
        System.out.println("随机内容包含chars。。。。"+RandomStringUtils.random(6,charArray));
        String chars="11月29日在美国休斯敦进行的2021世界乒乓球锦标赛女子双打决赛中中国组合孙颖莎王曼昱以3比0击败日本组合伊藤美诚早田希娜夺得冠军";
        System.out.println("随机内容包含chars。。。。"+RandomStringUtils.random(3,chars));
    }
}


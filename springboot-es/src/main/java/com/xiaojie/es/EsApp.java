package com.xiaojie.es;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Description:
 * @author: yan
 * @date: 2021.11.30
 */
@SpringBootApplication
@EnableSwagger2
@MapperScan("com.xiaojie.es.mapper")
public class EsApp {

    public static void main(String[] args) {
        SpringApplication.run(EsApp.class);
    }
}

package com.xiaojie.es.controller;

import com.xiaojie.es.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @Description:
 * @author: yan
 * @date: 2021.11.30
 */
@RestController
@Api(tags = "测试接口")
public class IndexController {

    @Autowired
    private UserService userService;
    @GetMapping("/index")
    @ApiOperation("首页接口")
    public String index(){
        return "success";
    }

    @GetMapping("/add")
    @ApiOperation("添加用户")
    @Async
    public String add() throws IOException {
        userService.add();
        return "success";
    }

    @GetMapping("/search")
    @ApiOperation("查询用户")
    public Object search() throws IOException {
       return  userService.search();
    }

}

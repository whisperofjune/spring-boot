package com.xiaojie.rocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description:
 * @author: yan
 * @date: 2021.11.08
 */
@SpringBootApplication
public class RocketMqApp {
    public static void main(String[] args) {
        SpringApplication.run(RocketMqApp.class);
    }
}

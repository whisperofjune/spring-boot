package com.xiaojie.rocket.rocket.producer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description: 发送消息：同步消息、异步消息和单向消息。其中前两种消息是可靠的，因为会有发送是否成功的应答
 * @author: yan
 * @date: 2021.11.08
 */
@Service
@Slf4j
public class MqProducer {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * @description: 单向发送
     *  这种方式主要用在不特别关心发送结果的场景，例如日志发送。
     * @param:
     * @return: void
     * @author xiaojie
     * @date: 2021/11/9 23:39
     */
    public void sendMq() {
        for (int i = 0; i < 10; i++) {
            rocketMQTemplate.convertAndSend("xiaojie-test", "测试发送消息》》》》》》》》》" + i);
        }
    }

    /**
     * @description: 同步发送
     * 这种可靠性同步地发送方式使用的比较广泛，比如：重要的消息通知，短信通知。
     * @param:
     * @return: void
     * @author xiaojie
     * @date: 2021/11/10 22:25
     */
    public void sync() {
        SendResult sendResult = rocketMQTemplate.syncSend("xiaojie-test", "sync发送消息。。。。。。。。。。");
        log.info("发送结果{}", sendResult);
    }

    /**
     * @description: 发送消息到同一个队列
     * @author xiaojie
     * @date 2021/11/22 23:12
     * @version 1.0
     */
    public void syncOrder() {
        SendResult sendResult = rocketMQTemplate.syncSendOrderly("xiaojie-test", "sync发送消息。。。。。。。。。。", "test-order");
        log.info("发送结果{}", sendResult);
    }

    /**
     * @description: 异步发送
     * 异步消息通常用在对响应时间敏感的业务场景，即发送端不能容忍长时间地等待Broker的响应。
     * @param:
     * @return: void
     * @author xiaojie
     * @date: 2021/11/10 22:29
     */
    public void async() {
        String msg = "异步发送消息。。。。。。。。。。";
        log.info(">msg:<<" + msg);
        rocketMQTemplate.asyncSend("xiaojie-test", msg, new SendCallback() {
            @Override
            public void onSuccess(SendResult var1) {
                log.info("异步发送成功{}", var1);
            }

            @Override
            public void onException(Throwable var1) {
                //发送失败可以执行重试
                log.info("异步发送失败{}", var1);
            }
        });
    }
}

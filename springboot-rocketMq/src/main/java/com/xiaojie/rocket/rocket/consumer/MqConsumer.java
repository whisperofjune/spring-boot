package com.xiaojie.rocket.rocket.consumer;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @author: yan
 * @date: 2021.11.08
 */
@RocketMQMessageListener(consumerGroup = "test-group", topic = "xiaojie-test")
@Slf4j
@Component
public class MqConsumer implements RocketMQListener<String> {

    @Override
    public void onMessage(String message) {
        log.info("接收到的数据是：{}", message);
    }
}

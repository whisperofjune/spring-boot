package com.xiaojie.test;

import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.util.Bytes;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/7 10:06
 */
public class Test {

    public static void main(String[] args) {
        String rowKey = StringUtils.reverse(new Date().getTime()+"");
        System.out.println(rowKey);
    }
}

package com.xiaojie.hadoop.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Arrays;

@Component
@Slf4j
public class HdfsClientUtil {

    @Value("${wssnail.hdfs.url}")
    private String url;

    @Value("${wssnail.hdfs.user-name}")
    private String userName;

    @Autowired
    private Configuration configuration;

    //创建文件
    public void mkDirs() throws URISyntaxException, IOException, InterruptedException {
        //获取文件系统
        FileSystem fs = FileSystem.get(new URI(url), configuration, userName);
        // 2 创建目录
        fs.mkdirs(new Path("/hello/world"));
        // 3 关闭资源
        fs.close();
        log.info("创建目录成功>>>>>>>>>>>>>>>");
    }

    //上传文件
    public void putFile() throws URISyntaxException, IOException, InterruptedException {
        //设置副本数
        configuration.set("dfs.replication", "2");
        FileSystem fs = FileSystem.get(new URI(url), configuration, userName);
        // 2 上传文件
        fs.copyFromLocalFile(new Path("d:/hello.txt"), new Path("/"));
        // 3 关闭资源
        fs.close();
    }

    //下载文件
    public void downloadFile() throws URISyntaxException, IOException, InterruptedException {
        // 1 获取文件系统
        FileSystem fs = FileSystem.get(new URI(url), configuration, userName);
        // 2 执行下载操作
        // boolean delSrc 指是否将原文件删除
        // Path src 指要下载的文件路径
        // Path dst 指将文件下载到的路径
        // boolean useRawLocalFileSystem 是否开启文件校验
        fs.copyToLocalFile(false, new Path("/hello.txt"), new Path("d:/hello1.txt"), true);

        // 3 关闭资源
        fs.close();

    }

    //移动
    public void renameFile() throws URISyntaxException, IOException, InterruptedException {
        // 1 获取文件系统
        FileSystem fs = FileSystem.get(new URI(url), configuration, userName);
        // 2 修改文件名称
        fs.rename(new Path("/hello.txt"), new Path("/hello1.txt"));
        // 3 关闭资源
        fs.close();

    }

    //删除
    public void deleteFile() throws URISyntaxException, IOException, InterruptedException {
        // 1 获取文件系统
        FileSystem fs = FileSystem.get(new URI(url), configuration, userName);
        // 2 删除
        fs.delete(new Path("/hello1.txt"), true);
        // 3 关闭资源
        fs.close();
    }

    //查看
    public void listFiles() throws URISyntaxException, IOException, InterruptedException {
        FileSystem fs = FileSystem.get(new URI(url), configuration, userName);
        RemoteIterator<LocatedFileStatus> listFiles = fs.listFiles(new Path("/"), true);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        while (listFiles.hasNext()) {
            LocatedFileStatus fileStatus = listFiles.next();

            System.out.println("========" + fileStatus.getPath() + "=========");
            System.out.println("文件权限:" + fileStatus.getPermission());
            System.out.println("所有者:" + fileStatus.getOwner());
            System.out.println("所属分组：" + fileStatus.getGroup());
            System.out.println("文件长度：" + fileStatus.getLen());
            System.out.println("文件修改时间：" + simpleDateFormat.format(fileStatus.getModificationTime()));
            System.out.println("副本数：" + fileStatus.getReplication());
            System.out.println("blockSize: " + fileStatus.getBlockSize() / 1024 / 1024 + "M");
            System.out.println("文件名称信息：" + fileStatus.getPath().getName());

            // 获取块信息
            BlockLocation[] blockLocations = fileStatus.getBlockLocations();
            System.out.println("块信息：" + Arrays.toString(blockLocations));
        }
        // 3 关闭资源
        fs.close();

    }

    //文件文件夹判断
    public void isFile() throws URISyntaxException, IOException, InterruptedException {
        FileSystem fs = FileSystem.get(new URI(url), configuration, userName);
        // 2 判断是文件还是文件夹
        FileStatus[] listStatus = fs.listStatus(new Path("/"));

        for (FileStatus fileStatus : listStatus) {

            // 如果是文件
            if (fileStatus.isFile()) {
                System.out.println("文件名称：" + fileStatus.getPath().getName());
            } else {
                System.out.println("目录名称：" + fileStatus.getPath().getName());
            }
        }
        // 3 关闭资源
        fs.close();

    }

}

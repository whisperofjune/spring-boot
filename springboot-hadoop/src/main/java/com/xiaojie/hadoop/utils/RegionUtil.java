package com.xiaojie.hadoop.utils;

import org.apache.hadoop.hbase.regionserver.IncreasingToUpperBoundRegionSplitPolicy;
import org.apache.hadoop.hbase.util.Bytes;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/7 6:11
 */
public class RegionUtil {

    /**
     * @param rowKey      rowkey
     * @param regionCount 分区个数
     * @description: 生成分区号
     * @return: java.lang.String
     * @author 熟透的蜗牛
     * @date: 2025/1/7 6:05
     */
    public static String genRegionNum(String rowKey, int regionCount) {
        int regionNum;
        int hash = rowKey.hashCode();
        if (regionCount > 0 && (regionCount & (regionCount - 1)) == 0) {
            regionNum = hash & (regionCount - 1);
        } else {
            regionNum = hash % regionCount;
        }
        return regionNum + "_" + rowKey;
    }


    /**
     * @param regionCount
     * @description: 生成分区键
     * @return: byte[][]
     * @author 熟透的蜗牛
     * @date: 2025/1/7 6:20
     */
    public static byte[][] genRegionKey(int regionCount) {
        byte[][] regionKey = new byte[regionCount - 1][];
        for (int i = 0; i < regionCount - 1; i++) {
            regionKey[i] = Bytes.toBytes(i + "|");
        }
        return regionKey;
    }


    public static void main(String[] args) {
//        String a1001 = genRegionNum("a1004", 5);
//        System.out.printf(a1001);
        byte[][] bytes = genRegionKey(5);
        for (int i = 0; i < bytes.length; i++) {
            System.out.println(new String(bytes[i]));
        }
    }


}

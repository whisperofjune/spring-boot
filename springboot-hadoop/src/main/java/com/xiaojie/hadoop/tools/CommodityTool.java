package com.xiaojie.hadoop.tools;

import com.xiaojie.hadoop.bean.CommodityBean;
import com.xiaojie.hadoop.mapper.CommodityMapper;
import com.xiaojie.hadoop.output.MysqlOutPutFormat;
import com.xiaojie.hadoop.reducer.CommodityReducer;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.JobStatus;
import org.apache.hadoop.util.Tool;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/7 22:18
 */
public class CommodityTool implements Tool {
    @Override
    public int run(String[] args) throws Exception {
        Job job = Job.getInstance();
        job.setJarByClass(CommodityTool.class);
        //设置mapper
        TableMapReduceUtil.initTableMapperJob("HBASE_COMMODITY", new Scan(),
                CommodityMapper.class, Text.class, CommodityBean.class, job);


        job.setReducerClass(CommodityReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(CommodityBean.class);


        job.setOutputFormatClass(MysqlOutPutFormat.class);

        return job.waitForCompletion(true) ? JobStatus.State.SUCCEEDED.getValue() : JobStatus.State.FAILED.getValue();
    }

    @Override
    public void setConf(Configuration conf) {

    }

    @Override
    public Configuration getConf() {
        return null;
    }
}

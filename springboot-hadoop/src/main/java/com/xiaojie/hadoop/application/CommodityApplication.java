package com.xiaojie.hadoop.application;

import com.xiaojie.hadoop.bean.CommodityBean;
import com.xiaojie.hadoop.mapper.CommodityMapper;
import com.xiaojie.hadoop.output.MysqlOutPutFormat;
import com.xiaojie.hadoop.reducer.CommodityReducer;
import com.xiaojie.hadoop.tools.CommodityTool;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.util.ToolRunner;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/7 22:17
 */
public class CommodityApplication {

    public static void main(String[] args) throws Exception {
        ToolRunner.run(new CommodityTool(), args);


        //本地调试

//        Configuration configuration = HBaseConfiguration.create();
//        //设置端口号
//        configuration.set("hbase.zookeeper.property.clientPort", "2181");
//        //设置zk连接
//        configuration.set("hbase.zookeeper.quorum", "hadoop1,hadoop2,hadoop3");
//        Job job = Job.getInstance(configuration);
//        //设置mapper
//        TableMapReduceUtil.initTableMapperJob("HBASE_COMMODITY", new Scan(),
//                CommodityMapper.class, Text.class, CommodityBean.class, job);
//
//
//        job.setReducerClass(CommodityReducer.class);
//        job.setOutputKeyClass(Text.class);
//        job.setOutputValueClass(CommodityBean.class);
//
//        job.setOutputFormatClass(MysqlOutPutFormat.class);
//        job.waitForCompletion(true);
//
//        System.exit(job.waitForCompletion(true) ? 1 : 0);
    }
}
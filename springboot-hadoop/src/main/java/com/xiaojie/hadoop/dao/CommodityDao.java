package com.xiaojie.hadoop.dao;

import com.xiaojie.hadoop.entity.CommodityLogBean;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: TODO
 * @date 2025/1/7 15:28
 */
@Mapper
public interface CommodityDao {


    @Select("select * from  hbase_commodity")
    List<CommodityLogBean> queryAll();

    @Insert("upsert into hbase_commodity  values(#{rowId} ,#{commodityId}, #{categoryId}, #{name},#{price},#{picture},#{userId},#{clickTime} )")
    void save(CommodityLogBean commodityLogBean);

    @Select("select * from hbase_commodity  where rowId=#{rowId} and categoryId=#{categoryId}")
    CommodityLogBean queryById(String rowId, String categoryId);


    @Delete("delete from hbase_commodity  where rowId=#{rowId} and userId=#{userId}")
    void deleteById(String rowId, String userId);

}

package com.xiaojie.hadoop.mapreduce.partitioner;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 定义一个bean 实现 writable接口
 * @date 2024/12/27 10:25
 */
public class FlowBean implements Writable {

    private long upFlow; //上行流量
    private long downFlow; //下行流量
    private long sumFlow; //总流量

    //创建无参构造函数
    public FlowBean() {
    }

    //创建gettter setter 方法
    public long getUpFlow() {
        return upFlow;
    }

    public void setUpFlow(long upFlow) {
        this.upFlow = upFlow;
    }

    public long getDownFlow() {
        return downFlow;
    }

    public void setDownFlow(long downFlow) {
        this.downFlow = downFlow;
    }

    public long getSumFlow() {
        return sumFlow;
    }

    public void setSumFlow(long sumFlow) {
        this.sumFlow = sumFlow;
    }

    //重写setSumFlow 方法
    public void setSumFlow() {
        this.sumFlow = this.upFlow + this.downFlow;
    }

    //重写序列化方法，输出和输入的顺序要保持一致
    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(upFlow);
        out.writeLong(downFlow);
        out.writeLong(sumFlow);

    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.upFlow = in.readLong();
        this.downFlow = in.readLong();
        this.sumFlow = in.readLong();

    }

    //结果显示在文本中，重写tostring 方法，
    @Override
    public String toString() {
        return upFlow + "\t" + downFlow + "\t" + sumFlow;

    }
}

package com.xiaojie.hadoop.mapreduce.comparable;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 流量mapper
 * @date 2024/12/27 10:32
 */
public class FlowBeanMapper extends Mapper<LongWritable, Text, FlowBean, Text> {

    FlowBean outKey = new FlowBean();
    Text outValue = new Text();

    @Override
    protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, FlowBean, Text>.Context context) throws IOException, InterruptedException {

        //获取一行
        String line = value.toString();
        //分隔数据
        String[] split = line.split("\t");

        //封装outk 和outValue
        outKey.setUpFlow(Long.parseLong(split[1]));
        outKey.setDownFlow(Long.parseLong(split[2]));
        outKey.setSumFlow();

        outValue.set(split[0]);

        //写出
        context.write(outKey, outValue);
    }
}
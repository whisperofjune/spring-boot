package com.xiaojie.hadoop.hbase;

import com.xiaojie.hadoop.utils.HBaseUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.SingleColumnValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.util.Pair;
import org.junit.Test;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 工具测试类，测试需要添加JVM参数，本人jdk为17 -ea
 * --add-opens=java.base/java.nio=ALL-UNNAMED
 * --add-opens=java.base/sun.nio.ch=ALL-UNNAMED
 * @date 2025/1/3 10:17
 */
@Slf4j
public class HBaseTest {

    private static final String STAFF_NAMESPACE = "staff_ns";
    private static final String TABLE_NAME = "staff";
    private static final String INFO = "info";
    private static final String POSITION = "position";


    @Test
    public void testCreateNameSpace() throws IOException {
        HBaseUtil.createNameSpace(STAFF_NAMESPACE);
    }

    @Test
    public void testDeleteNameSpace() throws IOException {
        HBaseUtil.deleteNameSpace(STAFF_NAMESPACE);
    }

    @Test
    public void testCreateTable() throws IOException {
        HBaseUtil.createTable("student", Arrays.asList(INFO), null);
    }


    @Test
    public void testDeleteTable() throws IOException {
        HBaseUtil.deleteTable("staff_ns:staff");
    }


    @Test
    public void testPutRow() throws IOException, NoSuchAlgorithmException {
        //1、加盐加密方式
//        String rowKey = ShaUtil.getSha1("1001");
        //2、hash
//        String rowKey = "a10001";
//        int h;
//        int result = Math.abs((rowKey == null) ? 0 : (h = rowKey.hashCode()) ^ (h >>> 16));

        //3、反转字符
        String row=new Date().toString();
        String rowkey= StringUtils.reverse(row);
        HBaseUtil.putRow(STAFF_NAMESPACE, TABLE_NAME, rowkey, INFO, "name", "tom");
    }

    @Test
    public void testPutRow1() throws IOException {
        List<Pair<String, String>> pairs1 = Arrays.asList(new Pair<>("name", "Tom"),
                new Pair<>("age", "30"),
                new Pair<>("gender", "1"), new Pair<>("salary", "200000"));

        HBaseUtil.putRow(STAFF_NAMESPACE, TABLE_NAME, "1002", INFO, pairs1);
        List<Pair<String, String>> pairs2 = Arrays.asList(new Pair<>("name", "托马斯"),
                new Pair<>("age", "39"),
                new Pair<>("gender", "0"), new Pair<>("salary", "45635"));
        HBaseUtil.putRow(STAFF_NAMESPACE, TABLE_NAME, "1003", INFO, pairs2);

        List<Pair<String, String>> pairs3 = Arrays.asList(new Pair<>("duty", "manager"),
                new Pair<>("years", "5"));
        HBaseUtil.putRow(STAFF_NAMESPACE, TABLE_NAME, "1003", POSITION, pairs3);
    }

    @Test
    public void testGetRowByRowKey() throws IOException {
        Result result = HBaseUtil.getRowByRowKey(null, "HBASE_COMMODITY", "8987277526371");

        for (Cell cell : result.rawCells()) {
            System.out.println("RowKey的值是：" + new String(result.getRow()));
            System.out.println("列族：" + Bytes.toString(CellUtil.cloneFamily(cell)));
            System.out.println("列标识：" + Bytes.toString(CellUtil.cloneQualifier(cell)));
            System.out.println("列值：" + Bytes.toString(CellUtil.cloneValue(cell)));
            System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
        }
    }

    @Test
    public void testGetAll() {
        ResultScanner scanner = HBaseUtil.getAll(STAFF_NAMESPACE, TABLE_NAME);
        Iterator<Result> iterator = scanner.iterator();
        while (iterator.hasNext()) {
            Result result = iterator.next();
            for (Cell cell : result.rawCells()) {
                System.out.println("RowKey的值是：" + new String(result.getRow()));
                System.out.println("列族：" + Bytes.toString(CellUtil.cloneFamily(cell)));
                System.out.println("列标识：" + Bytes.toString(CellUtil.cloneQualifier(cell)));
                System.out.println("列值：" + Bytes.toString(CellUtil.cloneValue(cell)));
            }
        }
        scanner.close();
    }

    @Test
    public void testGetByFilter() {
        FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
        SingleColumnValueFilter nameFilter = new SingleColumnValueFilter(Bytes.toBytes(INFO),
                Bytes.toBytes("name"), CompareOperator.EQUAL, Bytes.toBytes("Tom"));
        filterList.addFilter(nameFilter);
        ResultScanner scanner = HBaseUtil.getScanner(STAFF_NAMESPACE, TABLE_NAME, filterList);
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("name")))));
            scanner.close();
        }
    }

    @Test
    public void testGetCell() {
        String cell = HBaseUtil.getCell(STAFF_NAMESPACE, TABLE_NAME, "1003", "position", "years");
        System.out.println("years>>>>>>>>" + cell);
    }

    @Test
    public void testDeleteRow() {
        boolean b = HBaseUtil.deleteRow(STAFF_NAMESPACE, TABLE_NAME, "1001");
        System.out.printf(b == true ? "删除成功" : "删除失败");
    }

    @Test
    public void testDeleteColumn() {
        boolean b = HBaseUtil.deleteColumn(STAFF_NAMESPACE, TABLE_NAME, "1003", "position", "years");
        System.out.printf(b == true ? "删除成功" : "删除失败");
    }

    /*************************************************华丽的分割线*********************************************************************/
    @Test
    public void testGetDataByRowFilter() {
        ResultScanner scanner = HBaseUtil.getDataByRowFilter("staff_ns:staff", "1005");
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("name")))));
            scanner.close();
        }
    }

    @Test
    public void testGetDataByQualifierFilter() {
        ResultScanner scanner = HBaseUtil.getDataByQualifierFilter("staff_ns:staff", "duty");
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(POSITION), Bytes.toBytes("duty")))));
            scanner.close();
        }
    }

    @Test
    public void testGetDataByDependentColumnFilter() {
        ResultScanner scanner = HBaseUtil.getDataByDependentColumnFilter("staff_ns:staff", INFO, "age", "40");
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("name"))) + ">>>>>>>:" +
                    Bytes.toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("age")))));
            scanner.close();
        }
    }

    @Test
    public void testGetDataByValueFilter() {
        ResultScanner scanner = HBaseUtil.getDataByValueFilter("staff_ns:staff", "T");
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("name"))) + ">>>>>>>:" +
                    Bytes.toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("age")))));
            scanner.close();
        }
    }

    @Test
    public void testGetDataByFamilyFilter() {
        ResultScanner scanner = HBaseUtil.getDataByFamilyFilter("staff_ns:staff", "info");
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("name"))) + ">>>>>>>:" +
                    Bytes.toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("age")))));
            scanner.close();
        }
    }

    @Test
    public void testGetDataByPrefixFilter() {
        ResultScanner scanner = HBaseUtil.getDataByPrefixFilter("staff_ns:staff", "as");
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("name"))) + ">>>>>>>:" +
                    Bytes.toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("age")))));
            scanner.close();
        }
    }

    @Test
    public void testGetDataByColumnPrefixFilter() {
        ResultScanner scanner = HBaseUtil.getDataByColumnPrefixFilter("staff_ns:staff", "nam");
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("name"))) + ">>>>>>>:" +
                    Bytes.toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("age")))));
            scanner.close();
        }
    }

    @Test
    public void testGetDataByMultiFilter() {
        ResultScanner scanner = HBaseUtil.getDataByMultiFilter("staff_ns:staff", "as", INFO);
        if (scanner != null) {
            scanner.forEach(result -> System.out.println("RowKey>>>>>>>>>:" + Bytes.toString(result.getRow()) + "-------->:" + Bytes
                    .toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("name"))) + ">>>>>>>:" +
                    Bytes.toString(result.getValue(Bytes.toBytes(INFO), Bytes.toBytes("age")))));
            scanner.close();
        }
    }


    @Test
    public void testUseCoprocessor() throws IOException {
        HBaseUtil.useCoprocessor("staff_ns:staff", Arrays.asList(INFO, POSITION));
    }

    @Test
    public void testCreateTableRegion() throws IOException {
        boolean emp = HBaseUtil.createTableRegion("emp", Arrays.asList(INFO, POSITION), 5);
        log.info(emp == true ? "success" : "fail");
    }

    @Test
    public void testPutRegionRow() throws IOException {
        boolean flag = HBaseUtil.putRegionRow("emp", "a1001", INFO, "name", "James", 5);
        log.info(flag == true ? "success" : "fail");
    }


}

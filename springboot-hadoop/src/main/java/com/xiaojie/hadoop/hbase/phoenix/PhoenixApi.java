package com.xiaojie.hadoop.hbase.phoenix;

import java.sql.*;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 测试phoenix
 * 添加jvm参数
 * --add-opens=java.base/java.nio=ALL-UNNAMED
 * --add-opens=java.base/sun.nio.ch=ALL-UNNAMED
 * --add-opens=java.base/java.lang=ALL-UNNAMED
 * @date 2025/1/3 23:08
 */
public class PhoenixApi {

    /**
     * @description:注意 此代码在jdk17上运行会显示线程堆栈异常信息，但是不影响结果，目前还没解决，猜测是版本兼容问题，
     * 但是在java8中完美运行
     */

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        //1、加载驱动
        Class.forName("org.apache.phoenix.jdbc.PhoenixDriver");

        //2、创建连接
        String url = "jdbc:phoenix:hadoop1,hadoop2,hadoop3";
        Connection connect = DriverManager.getConnection(url);
        //3、创建查询
        PreparedStatement preparedStatement = connect.prepareStatement("SELECT * FROM us_population");

        //4、遍历结果
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            System.out.println(resultSet.getString("city") + " "
                    + resultSet.getInt("population"));
        }
        //5、关闭资源
        preparedStatement.close();
        resultSet.close();
        connect.close();
    }
}

package com.xiaojie.hadoop.hbase.split;

import org.apache.hadoop.hbase.regionserver.IncreasingToUpperBoundRegionSplitPolicy;

/**
 * @author 熟透的蜗牛
 * @version 1.0
 * @description: 重写拆分策略
 * @date 2025/1/7 9:46
 */
public class MyIncreasingToUpperBoundRegionSplitPolicy  extends IncreasingToUpperBoundRegionSplitPolicy {

    @Override
    protected boolean shouldSplit() {
        return super.shouldSplit();
    }
}

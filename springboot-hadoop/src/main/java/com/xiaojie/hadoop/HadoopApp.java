package com.xiaojie.hadoop;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.xiaojie.hadoop.hbase.phoenix.dao", "com.xiaojie.hadoop.dao"})
public class HadoopApp {

//    public static void main(String[] args) {
//        SpringApplication.run(HadoopApp.class, args);
//
//
//        /**
//         由于使用的是jdk17  ，需要添加以下jvm参数，java8不需要
//         --add-opens=java.base/java.nio=ALL-UNNAMED
//         --add-opens=java.base/sun.nio.ch=ALL-UNNAMED
//         --add-opens=java.base/java.lang=ALL-UNNAMED
//         */
//    }
}

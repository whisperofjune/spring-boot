package com.xiaojie.rocket.pojo;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 
 * @TableName tb_order
 */
@TableName(value ="tb_order")
@Data
public class Order implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 
     */
    private String orderid;

    /**
     * 
     */
    private String ordername;

    /**
     * 0-未支付，1-支付；2-过期
     */
    private Integer status;

    /**
     * 
     */
    @TableField(fill = FieldFill.INSERT)
    private Date createtime;

    /**
     * 
     */
    private Double paymoney;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

}
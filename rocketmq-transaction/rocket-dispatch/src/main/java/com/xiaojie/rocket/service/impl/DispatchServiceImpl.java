package com.xiaojie.rocket.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xiaojie.rocket.pojo.Dispatch;
import com.xiaojie.rocket.service.DispatchService;
import com.xiaojie.rocket.mapper.DispatchMapper;
import org.springframework.stereotype.Service;

/**
* @author Administrator
* @description 针对表【tb_dispatch】的数据库操作Service实现
* @createDate 2021-11-14 22:48:30
*/
@Service
public class DispatchServiceImpl extends ServiceImpl<DispatchMapper, Dispatch>
    implements DispatchService{

}





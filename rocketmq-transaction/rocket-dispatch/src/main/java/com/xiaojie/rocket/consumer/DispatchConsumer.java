package com.xiaojie.rocket.consumer;

import com.alibaba.fastjson.JSONObject;
import com.xiaojie.rocket.mapper.DispatchMapper;
import com.xiaojie.rocket.pojo.Dispatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 派单消费者
 * @date 2021/11/15 0:23
 */
@Component
@RocketMQMessageListener(consumerGroup = "order-consumer", topic = "order-topic-test")
@Slf4j
public class DispatchConsumer implements RocketMQListener<String> {

    @Autowired
    private DispatchMapper dispatchMapper;
    @Override
    public void onMessage(String msg) {
        log.info(">>>>>>>>>>>>>>>>>>>>>",msg);
        JSONObject jsonObject = JSONObject.parseObject(msg);
        String orderId = jsonObject.getString("orderId");
        // 计算分配的快递员id
        Dispatch dispatch=new Dispatch();
        dispatch.setOrderid(orderId);
        //经过一系列的算法得到送餐时间为30分钟
        dispatch.setSendtime(30*60L);
        dispatch.setRiderid(1000012L);
        dispatch.setUserid(15672L);
        // 3.插入我们的数据库
        int result = dispatchMapper.insert(dispatch);
    }
}

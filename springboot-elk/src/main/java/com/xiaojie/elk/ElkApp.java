package com.xiaojie.elk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/12/5 17:03
 */
@SpringBootApplication
public class ElkApp {
    public static void main(String[] args) {
        SpringApplication.run(ElkApp.class);
    }
}

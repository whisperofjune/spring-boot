package com.xiaojie.elk.controller;

import com.xiaojie.elk.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/12/5 16:45
 */
@RestController
public class IndexController {

    @Autowired
    private IndexService indexService;
    @GetMapping("/index")
    public String index(String name,String pwd){
        indexService.index(name,pwd);
        return  "用户名称："+name+"密码是：************";
    }
}

package com.xiaojie.elk.pojo;

import lombok.Data;

import java.util.Date;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/12/5 18:00
 */
@Data
public class RequestPojo {
    private String url;
    private String method;
    private String signature;
    private String args;
    private String  requestTime;
    private String address;
    private String error;
}

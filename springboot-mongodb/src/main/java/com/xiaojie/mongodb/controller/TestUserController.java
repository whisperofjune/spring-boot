package com.xiaojie.mongodb.controller;

import com.xiaojie.mongodb.model.TestUser;
import com.xiaojie.mongodb.service.TestUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @description: TODO
 * @author 熟透的蜗牛
 * @date 2023/1/6 0:05
 * @version 1.0
 */
@RestController
public class TestUserController {
    @Autowired
    private TestUserService testUserService;

    @GetMapping("/save")
    public String save() {
        testUserService.saveTestUser();
        return "success";
    }

    @GetMapping("/list/{page}/{pageSize}")
    public List<TestUser> list(@PathVariable int page, @PathVariable int pageSize) {
        return testUserService.listByPage(page, pageSize);
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable Long id) {
        testUserService.updateTestUser(id);
        return "success";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        testUserService.removeByUserId(id);
        return "success";
    }
}

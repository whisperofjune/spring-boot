package com.xiaojie.rabbitmq.direct;

import com.rabbitmq.client.*;
import com.xiaojie.rabbitmq.MyConnection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 路由键交换机
 * @date 2021/9/25 0:01
 */
public class Consumer {
    public static String EMAIL_QUEUE_FANOUT="email_queue";
    public static final  String EXCHANGE="my_direct_exchange";
    public static void main(String[] args) throws IOException, TimeoutException {
        //创建mq连接
        Connection connection = MyConnection.getConnection();
        //创建通道
        Channel channel = connection.createChannel();
        //创建队列 第二个参数true持久化
        channel.queueDeclare(EMAIL_QUEUE_FANOUT, true, false, false, null);
        //消费者队列绑定交换机 参数分别是 队列、交换机、routingkey
        channel.queueBind(EMAIL_QUEUE_FANOUT, EXCHANGE, "email");
        System.out.println("邮件消费者开启。。。。");
        //开启生产者监听
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String msg= new String(body,"utf-8");
                System.out.println("接收到的消息时msg："+msg);
            }
        };
        //设置应答模式
        channel.basicConsume(EMAIL_QUEUE_FANOUT,true, consumer);
    }
}

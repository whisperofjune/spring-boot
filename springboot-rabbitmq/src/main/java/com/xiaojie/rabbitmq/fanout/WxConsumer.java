package com.xiaojie.rabbitmq.fanout;

import com.rabbitmq.client.*;
import com.xiaojie.rabbitmq.MyConnection;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 微信消费者
 * 扇形交换机是通过同一个交换机，将消息处理到不同的队列，不同的队列对应不同的消费者
 * @date 2021/9/24 23:47
 */
public class WxConsumer {
    //交换机
    private static final  String EXCHANGE="my_fanout_exchange";
    //队列
    private  static final String WX_QUEUE="wx_queue";

    public static void main(String[] args) throws IOException, TimeoutException {
        Connection connection = MyConnection.getConnection();
        Channel channel = connection.createChannel();
        //创建队列
        channel.queueDeclare(WX_QUEUE, false, false, false, null);
        //绑定队列到交换机
        channel.queueBind(WX_QUEUE, EXCHANGE, "");
        System.out.println("微信消费者开启。。。。");
        //开启生产者监听
        DefaultConsumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String msg= new String(body,"utf-8");
                System.out.println("接收到的短信消息时msg："+msg);
            }
        };
        //设置应答模式 自动应答
        channel.basicConsume(WX_QUEUE,true, consumer);
    }
}

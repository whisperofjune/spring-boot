package com.xiaojie.score.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 积分实体类
 * @date 2021/10/10 21:50
 */
@Data
public class Score implements Serializable {
    private Long id;
    private String orderId;
    private Integer score;
    private Date createTime;
}

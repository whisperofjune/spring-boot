package com.xiaojie.score.controller;

import com.xiaojie.score.producer.ScoreProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/10/10 22:17
 */
@RestController
public class OrderController {

    @Autowired
    private ScoreProducer scoreProducer;
    /**
     * @description: 订单完成
     * @param:
     * @return: java.lang.String
     * @author xiaojie
     * @date: 2021/10/10 22:36
     */
    @GetMapping("/completeOrder")
    public String completeOrder(){
        return scoreProducer.completeOrder();
    }
}

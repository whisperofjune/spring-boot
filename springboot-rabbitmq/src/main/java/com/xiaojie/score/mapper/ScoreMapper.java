package com.xiaojie.score.mapper;

import com.xiaojie.score.entity.Score;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 积分
 * @date 2021/10/10 21:52
 */
@Repository
public interface ScoreMapper {
    @Select("select * from tb_score where orderId=#{orderId}")
    Score selectByOrderId(String orderId);

    @Insert("insert into tb_score(orderId,score,createTime) values(#{orderId},#{score},now())")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer save(Score score);
}

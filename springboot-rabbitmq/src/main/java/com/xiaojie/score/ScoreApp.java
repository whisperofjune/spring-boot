package com.xiaojie.score;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2021/10/10 21:48
 */
@SpringBootApplication
@MapperScan("com.xiaojie.score.mapper")
public class ScoreApp {
    public static void main(String[] args) {
        SpringApplication.run(ScoreApp.class);
    }
}

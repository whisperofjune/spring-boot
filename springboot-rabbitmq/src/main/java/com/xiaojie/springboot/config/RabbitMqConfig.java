package com.xiaojie.springboot.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 配置rabbitmq
 * @date 2021/9/25 22:16
 */
@Component
public class RabbitMqConfig {

    //定义队列
    private static final String MY_FANOUT_QUEUE = "xiaojie_fanout_queue";
    //定义队列
    private static final String MY_DIRECT_QUEUE = "xiaojie_direct_queue";
    //定义队列
    private static final String MY_TOPIC_QUEUE = "xiaojie_topic_queue";

    //定义fanout交换机
    private static final String MY_FANOUT_EXCHANGE = "xiaojie_fanout_exchange";
    //定义direct交换机
    private static final String MY_DIRECT_EXCHANGE = "xiaojie_direct_exchange";
    //定义topics交换机
    private static final String MY_TOPICS_EXCHANGE = "xiaojie_topics_exchange";

    //创建队列 默认开启持久化
    @Bean
    public Queue fanoutQueue() {
        return new Queue(MY_FANOUT_QUEUE);
    }

    //创建队列
    @Bean
    public Queue directQueue() {
        return new Queue(MY_DIRECT_QUEUE);
    }

    //创建队列
    @Bean
    public Queue topicQueue() {
        return new Queue(MY_TOPIC_QUEUE);
    }

    //创建fanout交换机 默认开启持久化
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange(MY_FANOUT_EXCHANGE);
    }

    //创建direct交换机
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(MY_DIRECT_EXCHANGE);
    }

    //创建direct交换机
    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange(MY_TOPICS_EXCHANGE);
    }

    //绑定fanout交换机
    @Bean
    public Binding fanoutBindingExchange(Queue fanoutQueue, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(fanoutQueue).to(fanoutExchange);
    }

    //绑定direct交换机
    @Bean
    public Binding directBindingExchange(Queue directQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(directQueue).to(directExchange).with("msg.send");
    }

    //绑定topic交换机
    @Bean
    public Binding topicBindingExchange(Queue topicQueue, TopicExchange topicExchange) {
        return BindingBuilder.bind(topicQueue).to(topicExchange).with("msg.#");
    }
    //多个队列绑定到同一个交换机
//    @Bean
//    public Binding topicBindingExchange1(Queue directQueue, TopicExchange topicExchange) {
//        return BindingBuilder.bind(directQueue).to(topicExchange).with("msg.send");
//    }
}

package com.xiaojie.springboot.myenum;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 订单状态
 * @date 2021/10/8 22:07
 */
public enum OrderStatus {
    UNPAY(0,"未支付"),
    PAY(1,"已支付"),
    EXPIRE(2,"已过期");
    private Integer status;
    private String statusText;

   private OrderStatus(Integer status, String statusText) {
        this.status = status;
        this.statusText = statusText;
    }

    public Integer getStatus() {
        return status;
    }

    public String getStatusText() {
        return statusText;
    }

}

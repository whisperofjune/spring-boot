package com.xiaojie.springboot.provider;

import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 生产者。模拟死信队列,消息过期后进入死信队列
 * @date 2021/10/8 21:28
 */
@Component
public class DLXProvider {

    //定义fanout交换机
    private static final String MY_DIRECT_EXCHANGE = "snail_direct_exchange";
    //普通队列路由键
    private static final String DIRECT_ROUTING_KEY = "msg.send";

    @Autowired
    private RabbitTemplate rabbitTemplate;
    public String sendDlxMsg(){
        String msg="我是模拟死信队列的消息。。。。。";
        rabbitTemplate.convertAndSend(MY_DIRECT_EXCHANGE, DIRECT_ROUTING_KEY, msg, (message) -> {
            //设置有效时间，如果消息不被消费，进入死信队列
//            message.getMessageProperties().setExpiration("30000");
            return message;
        });
        return "success";
    }

}

/*
 Navicat MySQL Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 80024
 Source Host           : localhost:3306
 Source Schema         : my_test

 Target Server Type    : MySQL
 Target Server Version : 80024
 File Encoding         : 65001

 Date: 09/10/2021 14:05:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `orderId` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `orderName` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL COMMENT '0-未支付，1-支付；2-过期',
  `createTime` datetime NULL DEFAULT NULL,
  `payMoney` double(10, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `orderId`(`orderId`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order` VALUES (7, 'b7ac431e-d725-4201-b1bf-568a897b81a8', 'test', 2, '2021-10-09 13:52:05', 3000.00);
INSERT INTO `tb_order` VALUES (8, 'ffda6453-5eb3-4cdd-a0b0-89a1b81d62b3', 'test', 1, '2021-10-09 13:52:08', 3000.00);
INSERT INTO `tb_order` VALUES (9, '9fc8cb08-bead-4014-b0d3-c98eac00744c', 'test', 2, '2021-10-09 13:52:09', 3000.00);

SET FOREIGN_KEY_CHECKS = 1;

package com.xiaojie.inteceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class LoginConfig implements WebMvcConfigurer {
	@Autowired
	private MyInteceptor myInteceptor;
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		final String [] excStrng={
				"/toLogin", // 登录
				"/**/*.html", // html静态资源
				"/**/*.js", // js静态资源
				"/**/*.css", // css静态资源
				"/**/*.woff",
				"/**/*.ttf",
				"/**/*.jpg",
				"/**/*.png",
				"/login"
		};
		InterceptorRegistration registration = registry.addInterceptor(myInteceptor);
		//拦截所有，过滤上面的请求
		registration.addPathPatterns("/**").excludePathPatterns( excStrng);
	}
}
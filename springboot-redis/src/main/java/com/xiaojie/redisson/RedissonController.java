package com.xiaojie.redisson;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaojie
 * @version 1.0
 * @description: 测试reddisson
 * @date 2022/3/31 0:28
 */
@RestController
@Slf4j
public class RedissonController {

    @Autowired
    private RedissonClient redissonClient;
    @RequestMapping("/testReddisson")
    public String testReddisson(Long id ){
        //获取reddisson锁
        RLock lock = null;
        try {
            lock = redissonClient.getLock(id+"");
            lock.lock();
            log.info("开始执行业务逻辑");
            return "success";
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            if (null!=lock){
                //释放锁
                lock.unlock();
            }
        }
        return "failure";
    }
}

package com.xiaojie.canal.example.rocketmq;

import com.xiaojie.canal.example.BaseCanalClientTest;

public abstract class AbstractRocektMQTest extends BaseCanalClientTest {

    public static String  topic              = "test-topic";
    public static String  groupId            = "test";
    public static String  nameServers        = "192.168.6.145:9876";
    public static String  accessKey          = "";
    public static String  secretKey          = "";
    public static boolean enableMessageTrace = false;
    public static String  accessChannel      = "local";
    public static String  namespace          = "";
}

/*
package com.xiaojie.canal;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.xiaojie.entity.User;
import com.xiaojie.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

*/
/**
 * @Description:解决redis与数据库数据不一致问题
 * @author: yan
 * @date: 2021.11.23
 *//*

@Component
@RocketMQMessageListener(consumerGroup = "canal1", topic = "canal-topic", consumeMode = ConsumeMode.ORDERLY
        , selectorType = SelectorType.TAG, selectorExpression = "*")
@Slf4j
public class CanalConsumer implements RocketMQListener<String> {

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public void onMessage(String message) {
        try {
            log.info("接收到的数据是：》》》》》》{}", message);
            JSONObject jsonObj = JSONObject.parseObject(message);
            JSONArray jsonArray = (JSONArray) jsonObj.get("data");
            if (null != jsonArray && jsonArray.size() > 0) {
                User user = JSONObject.parseObject(jsonArray.get(0).toString(), User.class);
                String database = jsonObj.getString("database");
                String table = jsonObj.getString("table");
                String type = jsonObj.getString("type");
                if (StringUtils.isEmpty(type)) {
                    log.info("不用同步数据。。。。。。。。。。。。。。");
                }
                if (type.equals("INSERT") || type.equals("UPDATE")) {
                    log.info("更新数据》》》》》》》》》》》》");
                    redisUtil.set(database + "_" + table + "_" + user.getId(), JSONObject.toJSONString(user), 24 * 60 * 60);
                } else {
                    if (redisUtil.hasKey(database + "_" + table + "_" + user.getId())) {
                        log.info("删除数据》》》》》》》》》》》》");
                        redisUtil.del(database + "_" + table + "_" + user.getId());
                    }
                }
            }
        } catch (Exception e) {
            log.info("数据更新异常》》》》》》》》》》》》", e);
        }

    }
}
*/

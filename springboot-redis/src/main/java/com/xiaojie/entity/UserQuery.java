package com.xiaojie.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/6/11 21:22
 */
@Data
public class UserQuery implements Serializable {

    private Integer start;
    private Integer end;
    private String name;
    private Integer id;
    private String shopCode;
    private String  scope;
}

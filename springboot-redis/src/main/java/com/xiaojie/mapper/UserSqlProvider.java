package com.xiaojie.mapper;

import com.xiaojie.entity.UserQuery;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.stereotype.Component;

/**
 * @author xiaojie
 * @version 1.0
 * @description: TODO
 * @date 2022/6/11 23:04
 */
public class UserSqlProvider {

    public String listUser(final UserQuery query){
        StringBuffer sql = new StringBuffer("select * from tb_user where 1=1 ");
        if(query.getName() != null){
            sql.append(" and name=#{name}");
        }
        if(query.getShopCode() != null){
            sql.append(" and shopCode=#{shopCode}");
        }
        return sql.toString();

    }
}

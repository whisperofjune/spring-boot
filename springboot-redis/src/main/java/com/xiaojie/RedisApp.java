package com.xiaojie;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Description:
 * @author: xiaojie
 * @date: 2021.09.08
 */
@SpringBootApplication
@MapperScan("com.xiaojie.mapper")
//@EnableCaching
@EnableScheduling
@EnableTransactionManagement
public class RedisApp {
    public static void main(String[] args) {
        SpringApplication.run(RedisApp.class);
    }
}

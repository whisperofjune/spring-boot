package com.xiaojie.service;

import com.xiaojie.entity.ActionScope;
import com.xiaojie.entity.User;
import com.xiaojie.entity.UserQuery;

import java.util.List;

/**
 * @Description:
 * @author: xiaojie
 * @date: 2021.09.08
 */
public interface UserService {

    User getUserByName(String name);

    List<User> getUSerList();

    /**
     * 把我们的数据提前预存到布隆过滤器中
     */
    void preBlongData();

    //分页查询用户列表
    List<User> list(UserQuery query, ActionScope scope);
}
